---
title: About this Blog
subtitle: The personal blog of Alexander Herwix, PhD student and researcher at the University of Cologne
comments: false
---

My name is Alexander Herwix and I am PhD student and researcher in Information Systems (IS) at the University of Cologne. I am currently working in an interdisciplinary project concerned with various aspects of the digital transformation of the education system in Germany. I am particularly interested in the development of teaching as a Design Science<sup>[1](#footnote1)</sup> and the requirements for new modes of cooperation between teachers, researchers, schools and universities that follow from this.  



<a name="footnote1">1</a>: Laurillard, D. (2013). Teaching as a design science: Building pedagogical patterns for learning and technology: Routledge.
